{-# LANGUAGE Safe #-}

module Utils where

(|>) a b = b $ a

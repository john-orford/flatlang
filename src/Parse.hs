{-# LANGUAGE Safe #-}
{-# LANGUAGE FlexibleContexts #-}

module Parse where

import Language.Haskell.TH.Syntax
import Text.Parsec.Prim (ParsecT(..))
import Data.Functor.Identity
import Text.ParserCombinators.Parsec

import Utils
import Data.List


eol :: GenParser Char state ()
eol = (char '\n' <|> (char '\r' >> option '\n' (char '\n'))) >> return ()


-- add lambda for run parser function
parseDecl :: Monad m => (String, Int, Int) -> String -> m [((String, [] String), Dec)]
parseDecl (file, line, col) s =
    case runParser p () "" s of
      Left err  -> fail $ show err
      Right e   -> return e
  where
    p = do  pos <- getPosition
            setPosition $
              (flip setSourceName) file $
              (flip setSourceLine) line $
              (flip setSourceColumn) col $
              pos
            spaces
            ds <- decls
            eof
            return $ ds



decls = many decl

decl :: ParsecT [Char] u Identity ((String, [] String), Language.Haskell.TH.Syntax.Dec)
decl = lexeme $ do{ skipMany $ comment <|> eol;
                    id <- ident;
                    symbol "=";
                    (edges, e) <- expr;
                    skipMany $ comment <|> eol;
                    return ( (id, edges), FunD (mkName id) [Clause [] (NormalB e) []] )
                  }

-- comment :: GenParser Char state ()
comment = do string "--"
             manyTill anyChar (eol <|> try eof)
             return ()

expr = app <|> try ( parens (app <|> lam ) ) <|> lam <|> var <|> integer




app = try app_infix <|> try app_normal

-- app as much as args available
app_normal = lexeme $ do{
  -- one of:
  -- (\ x -> x) 1 // lambda applied to 1
  -- (f 1) 1 // f returns a function and is applied to 1
  -- f a
  (f', f) <- parens (lam <|> try app_normal) <|> var;
  -- parse _all_ the args -- need at least one arg to successfully 'app'
  es <- many1 (parens expr <|> expr);
  -- do the applying
  return $ (
      foldl (\ a c -> c ++ a) f' (map (\ (es', _) -> es') es),
      foldl (\ a c -> AppE a c)
        (AppE f (es |> head |> snd))
        (es |> tail |> map (\ (_, ex') -> ex')
        )
      )
  }

var = lexeme $ do{
  i <- ident;
  return $ ( [i], VarE $ mkName i)
  }

lam = lexeme $ do{
  i <- symbol "\\" *> ident;
  symbol "->";
  (edges, e) <- expr;
  return $ ( nub edges \\ nub [i], LamE [VarP $ mkName i] e )
  }

integer  = lexeme $ do{
  ds <- (plus <|> minus <|> number);
  return $ ( [], LitE $ IntegerL $ read ds )
  }

-- 1 + (\ x -> x)
-- how  to stop this from type checking?
-- essentially GHC is trying to:
-- :: f + f


-- app version which doesn't allow partial app

app_infix = try addOp <|> try subtractOp <|> try multiplyOp <|> try divideOp

addOp = op' "+"
multiplyOp = op' "*"
subtractOp = op' "-"
divideOp = op' "/"

op' s = lexeme $ do{
  i <- (parens app <|> var <|> integer)
    |> optionMaybe;
  symbol s;
  i' <- ( parens (app <|> var <|> integer) <|> try app_normal <|> try app_infix <|> var <|> integer )
    |> optionMaybe;
  ( ((getNodes i) ++ (getNodes i')), (InfixE (getExp i) (VarE $ mkName $ "Ops." ++ s) (getExp i') ) )
  |> return
  }

getExp = fmap (\ (_, e) -> e)

getNodes m =
  m
  |> fmap (\ (ns, _) -> ns)
  |> \ x -> case x of
              Nothing -> []
              Just ns -> ns





-----------------------------------------------------
-- shared

plus   = symbol "+" *> number
minus  = (++) <$> symbol "-" <*> number
number = many1 digit

symbol name = lexeme $ string name

parens = between (symbol "(") (symbol ")")

idchar  = small <|> large <|> digit

small   = lower
large   = upper
  
ident  :: CharParser s String
ident  =  lexeme $ do{
  c <- small;
  cs <- many idchar;
  return (c:cs)
  }

lexeme p = do{
  x <- p;
  skipMany $ string " " ;
  return x
  }

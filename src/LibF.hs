{-# LANGUAGE QuasiQuotes, TemplateHaskell #-}
{-# LANGUAGE DeriveDataTypeable #-}

module LibF where

import Utils
import Language.Haskell.TH as TH
import Language.Haskell.TH.Quote

import Parse
import LibTotal (cycleCheck, buildGraph)
import Text.ParserCombinators.Parsec

import Data.Graph.Inductive.PatriciaTree

qQ_f = quoteFile qQ

qQ :: QuasiQuoter
qQ = QuasiQuoter
  { quoteDec = quoteDecl
  , quoteExp  = error "must be used as a declaration"
  , quotePat  = error "must be used as a declaration"
  , quoteType = error "must be used as a declaration"
  }

quoteDecl :: String -> Q [Dec]
quoteDecl s =  do  loc <- TH.location
                   let pos = (TH.loc_filename loc,
                              fst (TH.loc_start loc),
                              snd (TH.loc_start loc)
                             )
                   decls <- Parse.parseDecl pos s
                   case ( decls |> LibTotal.cycleCheck ) of
                     False -> (map (\(_, th) -> th ) decls) |> return
                     _ -> error "Cycle Check Failed"

-- see where cycle is
showGraph :: [Char] -> Maybe (Gr () ())
showGraph s = s |> parse Parse.decls ""
  |> (\ a -> case a of
               Right bb -> bb |> LibTotal.buildGraph |> Just
               _ -> Nothing
       )








{-# LANGUAGE Safe #-}
--{-# LANGUAGE FlexibleInstances #-}

module Ops where


(/) a b = case b of
  0 -> Nothing
  _ -> Just $ Prelude.div a b

(+) = (Prelude.+)
(-) = (Prelude.-)
(*) = (Prelude.*)




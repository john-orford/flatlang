module LibTotal where

import Data.List
import Text.ParserCombinators.Parsec

import Data.Graph.Inductive.Graph
import Data.Graph.Inductive.PatriciaTree
import Data.Graph.Inductive.Query.DFS

import Parse (decls)
import Utils

cycleCheck :: [((String,[String]), b1)] -> Bool
cycleCheck ds = ds |> buildGraph |> hasCycle

-- has cycle if no of edges & components is greater than number of nodes
hasCycle :: Gr () () -> Bool
hasCycle g = (size g) + (g |> noComponents) > (order g) 

buildGraph :: [((String,[String]), b1)] -> Gr () () 
buildGraph ds = ds
  |> parseD
  |> \ (nodes, edges) -> mkUGraph nodes  ( edges |> Data.List.nub )

parseD :: [((String,[String]), b1)] -> ([Node], [Edge])
parseD ds = ds
           |> map (\(a, _) -> a)
           |> zipWith (\i (nodeName, out) -> (i, nodeName, out)) [0..]
           |> labelsToNumbers
           |> foldl (\ (nodes, edges) (nodeNumber, out) -> (nodeNumber : nodes, out ++ edges)) ([],[])

labelsToNumbers :: (Eq b, Eq a) => [(a, b, [b])] -> [(a, [(a, a)])]
labelsToNumbers ns = ns
        |> map (\(nodeNumber, nodeName, out) ->
                   ( nodeNumber
                   , out
                     |> map (\label -> find' ns label)
                     |> filter (\n -> n /= Nothing)
                     |> map (\(Just i) -> (nodeNumber,i))
                   )
               )

find' :: (Foldable t, Eq a1) => t (a2, a1, c) -> a1 -> Maybe a2
find' nodes label = nodes
  |> find (\(nr,nm,os) -> nm == label)
  |> ( \ a' ->
         case a' of
           Just (nr,nm,os) -> Just nr
           _ -> Nothing
     )









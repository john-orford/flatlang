{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts #-}

module Rfnd where

import Refined


o :: Refined NonZero Integer 
o = $$(refineTH 1)

t :: Refined NonZero Integer 
t = $$(refineTH 2)

z :: Refined IdPred Integer 
z = $$(refineTH 0)


div' :: Predicate p Integer => Refined p Integer -> Refined NonZero Integer -> Integer
div' a b = div (unrefine a) (unrefine b)

sub' :: Predicate p Integer => Refined p Integer -> Refined p Integer -> Integer
sub' a b = (Prelude.-) (unrefine a) (unrefine b)


test :: Integer
test = sub' o o

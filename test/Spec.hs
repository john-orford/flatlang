{-# LANGUAGE QuasiQuotes, TemplateHaskell #-}

import LibF
import Language.Haskell.TH
import Ops

import Test.Hspec
 
[qQ_f|test/test.fl|]

main = hspec $ do
  describe "absolute" $ do

    it "returns Just 2" $
      xoxo `shouldBe` Just 2

